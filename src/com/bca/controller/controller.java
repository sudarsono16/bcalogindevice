package com.bca.controller;

import adapter.ErrorAdapter;
import model.mdlAPIResult;
import model.mdlResult;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import adapter.DeviceManagementAdapter;
import adapter.LogAdapter;

import javax.servlet.http.HttpServletResponse;

@RestController
public class controller {
    final static Logger logger = LogManager.getLogger(controller.class);
    static Gson gson = new Gson();

    @RequestMapping(value = "/ping", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String GetPing(HttpServletResponse response) {
        mdlAPIResult mdlPingResult = new mdlAPIResult();
        mdlResult mdlResult = new mdlResult();
        mdlResult.Result = "OK";
        mdlPingResult.ErrorSchema = ErrorAdapter.getErrorSchemaPing("00");
        mdlPingResult.OutputSchema = mdlResult.Result;
        response.setStatus(HttpServletResponse.SC_OK);
        return gson.toJson(mdlPingResult);
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST, consumes = "application/json", headers = "Accept=application/json")
    public @ResponseBody model.mdlAPIResult LoginDeviceManagement(@RequestBody model.mdlLoginDeviceManagement param) {
        long startTime = System.currentTimeMillis();
        String systemFunction = Thread.currentThread().getStackTrace()[1].getMethodName();
        model.mdlAPIResult mdlLoginDeviceManagementResult = new model.mdlAPIResult();
        model.mdlErrorSchema mdlErrorSchema = new model.mdlErrorSchema();
        model.mdlMessage mdlMessage = new model.mdlMessage();
        model.mdlResult mdlResult = new model.mdlResult();

        model.mdlLog mdlLog = new model.mdlLog();
        mdlLog.WSID = "";
        mdlLog.SerialNumber = param.SerialNumber;
        mdlLog.ApiFunction = "loginDeviceManagement";
        mdlLog.SystemFunction = systemFunction;
        mdlLog.LogSource = "Webservice";
        mdlLog.LogStatus = "Failed";

        mdlResult.Result = "false";

        String UserID = (param.UserID == null) ? "" : param.UserID.trim();
        String Password = (param.Password == null) ? "" : param.Password.trim();
        Boolean checkLogin = false;

        if (UserID.equals("") || Password.equals("")) {
            mdlErrorSchema.ErrorCode = "01";
            mdlMessage.Indonesian = "User ID atau Password invalid";
            mdlMessage.English = mdlLog.ErrorMessage = "Invalid User ID or Password";
            mdlErrorSchema.ErrorMessage = mdlMessage;
            mdlLoginDeviceManagementResult.ErrorSchema = mdlErrorSchema;
            mdlLoginDeviceManagementResult.OutputSchema = mdlResult;
            LogAdapter.InsertLog(mdlLog);
            return mdlLoginDeviceManagementResult;
        } else {
            try {
                checkLogin = DeviceManagementAdapter.CheckLogin(param);
                if (checkLogin) {
                    mdlErrorSchema.ErrorCode = "00";
                    mdlMessage.Indonesian = "Berhasil";
                    mdlMessage.English = mdlLog.LogStatus = "Success";
                    mdlErrorSchema.ErrorMessage = mdlMessage;
                    mdlLoginDeviceManagementResult.ErrorSchema = mdlErrorSchema;
                    mdlResult.Result = checkLogin.toString();
                    mdlLoginDeviceManagementResult.OutputSchema = mdlResult;
                } else {
                    mdlErrorSchema.ErrorCode = "02";
                    mdlMessage.Indonesian = "Password yang Anda masukkan salah";
                    mdlMessage.English = mdlLog.ErrorMessage = "Wrong Password Input";
                    mdlErrorSchema.ErrorMessage = mdlMessage;
                    mdlLoginDeviceManagementResult.ErrorSchema = mdlErrorSchema;
                    mdlLoginDeviceManagementResult.OutputSchema = mdlResult;
                }

                String jsonIn = gson.toJson(param);
                logger.info(LogAdapter.logControllerToLog4j(true, startTime, 200, "BCALoginDevice", "POST", "", jsonIn, gson.toJson(mdlLoginDeviceManagementResult)));
            } catch (Exception ex) {
                mdlErrorSchema.ErrorCode = "03";
                mdlMessage.Indonesian = "Gagal";
                mdlMessage.English = mdlLog.ErrorMessage = "Failed";
                mdlErrorSchema.ErrorMessage = mdlMessage;
                mdlLoginDeviceManagementResult.ErrorSchema = mdlErrorSchema;
                mdlLoginDeviceManagementResult.OutputSchema = mdlResult;
                String jsonIn = gson.toJson(param);
                logger.error(LogAdapter.logControllerToLog4jException(startTime, 500, "BCALoginDevice", "POST", "", jsonIn, gson.toJson(mdlLoginDeviceManagementResult), ex.toString()), ex);
            }
        }
        LogAdapter.InsertLog(mdlLog);
        return mdlLoginDeviceManagementResult;
    }

}
