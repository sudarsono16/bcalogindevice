package model;

public class mdlLog {
	public String LogID;
	public String LogDate;
	public String LogSource;
	public String WSID;
	public String SerialNumber;
	public String ApiFunction;
	public String LogStatus; //success or failed
	public String ErrorMessage; //filled if logStatus = failed
	public String SystemFunction;
}