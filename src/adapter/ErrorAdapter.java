package adapter;

import model.mdlErrorSchema;
import model.mdlMessage;

public class ErrorAdapter {
    public static mdlErrorSchema getErrorSchemaPing(String errorCode) {
        mdlErrorSchema mdlErrorSchema = new mdlErrorSchema();
        mdlErrorSchema.ErrorCode = errorCode;
        mdlMessage mdlMessage = new mdlMessage();
        if ("00".equals(errorCode)) {
            mdlMessage.Indonesian = "Sukses";
            mdlMessage.English = "Success";
        } else {
            mdlErrorSchema.ErrorCode = "99";
            mdlMessage.Indonesian = "Gagal memanggil service";
            mdlMessage.English = "Service call failed";
        }
        mdlErrorSchema.ErrorMessage = mdlMessage;
        return mdlErrorSchema;
    }
}
